﻿using System;

using Xamarin.Forms;

namespace TestMVVM
{
	public class App : Application
	{
		public App ()
		{
			var page = new HomePage ();
			page.BindingContext = new HomeViewModel ();

			MainPage = page;
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

