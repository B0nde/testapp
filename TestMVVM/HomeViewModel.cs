﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace TestMVVM
{
	public class HomeViewModel : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		private string text;
		public string Text
		{
			get { return text; }
			set
			{
				text = value;
				Notify ("Text");
			}
		}

		public ICommand ButtonCmd { get; private set; }

		private int clicks;

		public HomeViewModel ()
		{
			ButtonCmd = new Command (() => 
				{
					Text = string.Format("Button clicked {0} times.", ++clicks);
				});
		}

		private void Notify(string property)
		{
			if(PropertyChanged != null)
			{
				PropertyChanged (this, new PropertyChangedEventArgs(property));
			}
		}
	}
}

